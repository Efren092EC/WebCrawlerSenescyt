/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.gestiondatos.crawlersenescyt;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Efren Narvaez
 */
public class Titulos {

    public Titulos() {
    }

    public Document Page(String identificacion) throws IOException, SocketTimeoutException {
        Document doc = null;
        boolean success = false;
        int i = 0;
        while (i < 10) {
            try {
                String url = "http://www.senescyt.gob.ec/web/guest/consultas/";
                doc = Jsoup.connect(url).userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36")
                        .data("identificacion", identificacion).post();
                success = true;
                break;
            } catch (SocketTimeoutException ex) {
                System.out.println("Error de conexion");
            } catch (IOException ex) {
                ex.printStackTrace();
                i++;
            }
            if (success) {
                // Selector code ...
                return doc;
            }
        }
        return doc;
    }

    /*
            Optener el Numero de Tablas existentes por el registro y de esas 
            forma identificar cada uno de los niveles academicos a los que 
            pertenecen los titulos
     */
    public int NivelAcademico(Document doc) throws IOException {
        int numtables = 0;
        Elements tables = doc.select("table");
        for (Element table : tables) {
            numtables++;
        }
        return numtables;
    }

    /*
            Optener todos los titulos de  y sus detalles
     */
    public ArrayList ExtraerTitulos(Document doc, int n) {
        ArrayList<String> titulos = new ArrayList<String>();
        Element tablatercernivel = doc.select("table").get(n);
        Elements tableRowElements = tablatercernivel.select(":not(thead) tr");
        for (int i = 0; i < tableRowElements.size(); i++) {
            Element row = tableRowElements.get(i);
            Elements rowItems = row.select("td");
            for (int j = 0; j < rowItems.size(); j++) {
                titulos.add(rowItems.get(j).text());
            }
        }
        return titulos;
    }

    public List InformacionPersonal(Document doc) {
        Elements divs = doc.getElementsByClass("panel-body");
        for (Element elem : divs) {
            System.out.println(elem.getElementsByClass("col-xs-8")); //get all elements inside div
        }
        return null;
    }

}
