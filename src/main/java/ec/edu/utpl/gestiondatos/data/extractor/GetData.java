package ec.edu.utpl.gestiondatos.data.extractor;

import ec.edu.utpl.gestiondatos.data.source.connection.Oracle;
import java.sql.ResultSet;

/**
 *
 * @author Efren Narvaez
 */
public class GetData {

    Oracle d = new Oracle("172.16.xxx.xx", "ESQ_SIEC", "xxx", "1521", "xxx");

    public ResultSet GetData(String consulta) {
        ResultSet resultado = null;
        System.out.println("Conectando.... con la base de datos:");
        if (d.conectar()) {
            resultado = d.ejecutarConsulta(consulta);
            return resultado;
        } else {
            System.out.println("no hay conexion");
        }
        if (d.desconectar()) {
            System.out.println("Desconectado tras ejcutar la consulta.");
        } else {
            System.out.println("Por alguna razon no se ha podido desconectar.");
        }
        return resultado;
    }
    
      public void InsertData(String consulta) {       
        System.out.println("Conectando.... con la base de datos:");
        if (d.conectar()) {
            d.ejecutarConsulta(consulta);
           
        } else {
            System.out.println("No se pudo conectar. Revisa los datos introducidos.");
        }
        if (d.desconectar()) {
            System.out.println("Desconectado tras ejcutar el insert.");
        } else {
            System.out.println("Por alguna razon no se ha podido desconectar.");
        }
    
    }

}
