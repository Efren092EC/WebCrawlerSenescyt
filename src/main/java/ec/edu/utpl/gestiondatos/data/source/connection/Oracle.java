package ec.edu.utpl.gestiondatos.data.source.connection;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Oracle {

    private String host;
    private String user;
    private String password;
    private String port;
    private String dir;
    private Statement stmt;
    private Connection con;

    public Oracle() {

    }

    public Oracle(String h, String u, String p, String po, String s) {
        port = po;
        host = h;
        password = p;
        dir = s;
        user = u;
    }

    public boolean conectar() {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            con = DriverManager.getConnection("jdbc:oracle:thin:@" + host + ":" + port + ":" + dir, user, password);
            stmt = con.createStatement();
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        } catch (SQLException e) {
            return false;
        }
    }

    public ResultSet ejecutarConsulta(String consulta) { 
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery(consulta);
            return rs;
        } catch (Exception SQLException) {
            System.out.println("Salto la escepcion\n" + SQLException);
        }
        return rs;
    }
    
       public void ejecutarInsert (String consulta) { 
    
        try {
            stmt.executeUpdate(consulta);
         
        } catch (Exception SQLException) {
            System.out.println("Salto la escepcion\n" + SQLException);
        }
       
    }

    public boolean desconectar() {
        try {
            con.close();
            return true;
        } catch (Exception SQLException) {
            return false;
        }
    }
}
