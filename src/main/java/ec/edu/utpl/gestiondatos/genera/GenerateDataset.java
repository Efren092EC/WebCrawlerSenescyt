/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.gestiondatos.genera;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ec.edu.utpl.gestiondatos.data.extractor.GetData;
import ec.edu.utpl.gestiondatos.crawlersenescyt.Titulos;
import java.io.IOException;
import java.util.ArrayList;
import org.jsoup.nodes.Document;

/**
 *
 * @author Efren
 */
public class GenerateDataset {

    public GenerateDataset() {

    }

    public void DocenteDataSet() throws SQLException, IOException {
        Titulos tit = new Titulos();
        try {
            int cuantos = 0;
            GetData get = new GetData();
            ResultSet resultado = get.GetData("SELECT DISTINCT DO.DO_IDENTIFICACION\n"
                    + "FROM TDI_DOCENTE DO\n"
                    + "WHERE NOT EXISTS\n"
                    + "  (SELECT * FROM TDI_FORMACION_ACADEMICA_DOC FD WHERE FD.DO_ID = DO.DO_ID\n"
                    + "  )");
            while (resultado.next()) {
                cuantos++;
                Document doc = tit.Page(resultado.getString(1));
                int ntables = tit.NivelAcademico(doc);
                int nivel = 0;
                int grado = 0;
                int tama = 0;
                ArrayList<String> titulos = new ArrayList<String>();
                for (int i = 0; i < ntables; i++) {
                    int contador = 0;
                    nivel++;
                    titulos = tit.ExtraerTitulos(doc, i);
                    tama = titulos.size() / 6;
                    for (int x = 0; x < tama; x++) {
                        grado = 4 - nivel;
                        get.InsertData("INSERT INTO TDI_TITULOS_AUXILIAR_10112016 VALUES (" + "'" + resultado.getString(1) + "'" + "," + "'" + titulos.get(contador) + "'" + "," + "'" + titulos.get(1) + "'" + "," + "'" + titulos.get(2) + "'" + "," + "'" + titulos.get(3) + "'" + "," + "'" + titulos.get(4) + "'" + "," + "'" + titulos.get(5) + "'" + "," + "'" + titulos.get(6) + "'" + "," + "'" + grado + "'" + ")");
                        contador = contador + 7;
                    }

                }
                System.out.println("Ingresados: " + cuantos + " de 1947 ");
            }
        } catch (SQLException ex) {
            Logger.getLogger(GenerateDataset.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
